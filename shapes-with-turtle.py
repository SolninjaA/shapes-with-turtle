'''
    Author: SolninjaA
    Project Name: shapes
    Course: Python #1
    YouTube Channel: SolninjaA
    Twitch Channel: iamsolninjaa
'''
# import the turtle library
import turtle

# define a turtle (Bob) and set it's shape to a turtle
bob = turtle.Turtle()
bob.shape("turtle")

# define to check if user wants to keep drawring shapes
done = False

while not done:
    # defining variables
    print("-------Draw custom shapes V2------")

        # show Bob
    bob.showturtle()

        # ask the user for the number of lines, length of lines and number of degrees to turn as inputs
    shape = input('What shape do you want to draw? eg. customshape\n(for a full list of shapes that are supported visit: \nhttps://solninjaa.weebly.com/shapesv2shapelist.html\n!NOTE! Press enter to use custom angles eg. 56, 60 and 50: ')
    

        # (This is over simplified)
        # the main code, the if statement below asks if you answered
        # "square" when it asked you what
        # shape you would like to draw and draws it!
        
    
    if shape == "square":
        print("Working...")
        for i in range(4):
            bob.forward(100)
            bob.left(90)
            

                
    # (This is over simplified)
    # asks if you answered "customshape" when it asked you what shape you would
    # like to draw then draws it.
    
        
    elif shape == "customshape":
        print("Working...")
        for c in range(56):
            bob.forward(60)
            bob.left(50)
        
    # if you answer with enter it will use custom inputs
    elif shape == "":
        lines = input("How many lines does the shape have?: ")     
        length = input("What is the length of the lines?: ")
        angle = input("How much to turn after each line?: ")
        
        lines = int(lines)
        length = int(length)
        angle = int(angle)
        print("Working...")
        for b in range(lines):
                bob.forward(length)
                bob.left(angle)

            

        

    else:
        print("Sorry something went wrong you may have mispelt something or used a shape that is not yet supported. Please check your spelling.\n")
        
        
    # hides Bob (the turtle)
    bob.hideturtle()
    # asks if you want to clear the stage
    input("Press any key to clear: ")
    cont = input("Do you want to continue? Y/y: ")

    if cont != "Y" and cont != "y":
        cont2 = input("Are you sure you want to quit: ")
        if cont2 == "Y" or cont2 == "y":
                done = True
        
    bob.clear()
    # resets the position of bob (the turtle)       
    bob.reset()
